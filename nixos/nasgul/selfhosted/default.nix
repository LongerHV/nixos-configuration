{
  imports = [
    ./auth.nix
    ./dashboard.nix
    ./lldap.nix
    ./monitoring.nix
    ./nameserver.nix
    ./wireguard.nix
  ];
}
