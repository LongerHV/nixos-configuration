# NASgul

Personal NAS server

## Features

- Mirrored ZFS boot drives

- 4x4TB hard drive Raid-Z1 array

- Traefik reverse proxy (with SSL)

- Blocky local DNS server with adblocking

- LLDAP authentication server

- Authelia SSO (using LLDAP as backend)

- Dashy dashboard

- Monitoring using Prometheus and Grafana

- Backups to S3 object storage with Restic

- MariaDB

- Redis cache/session provider

- Nextcloud

- Gitea

- Jellyfin media server

- Sonarr/Radarr/Bazarr/Prowlarr media management

- Deluge BitTorrent client (routed through wireguard)

- SMTP + Sendmail (through SendGrid)

- NixOS cache
