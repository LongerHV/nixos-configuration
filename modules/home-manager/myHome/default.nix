{
  imports = [
    ./cli.nix
    ./devops.nix
    ./gnome
    ./neovim
    ./non-nixos.nix
    ./tmux.nix
    ./zsh.nix
  ];
}
